console.log("Hello, World!");

// [SECTION] While Loop

/*
	Syntax 
		While (expression/condition) {
			statement
		}
*/

// Whiel the value of count is not equal to 0

let countup = 0;

while(countup <= 5) {
	console.log("While Up: " + countup);

	// iteration - it increases the value of count until it reaches 5
	countup++;
}


let countdown = 5;

while (countdown >= 0) {
	console.log("While Down: " + countdown);
	countdown--;
}

// [SECTION] Do While Loop

// A do while loop works a lot like the while loop. But unlike while loops, do-while loops guarantee that a code will be executed at least once.

/*
	Syntax
	do {
		statement
	}
	while (expression/condition);
*/

let number = Number(prompt("Give me a number"));

do {
	console.log("Do While: " + number);
	number += 1;
}
while (number <= 10);

// [SECTION] For Loops

/*
	Syntax
	for (initialization; expression/condition; iteration) {
		statement
	}
*/

for (let i = 0; i <=20; i++) {
	console.log(i);
}

let myString = "Chikitito";

// console.log(myString.length);

// console.log(myString[0]);
// console.log(myString[1]);
// console.log(myString[2]);
// console.log(myString[3]);

for (let i = 0; i < myString.length; i++) {
	console.log(myString[i]);
}

let array = [];
let myName = prompt("What's your first name?")	

for (let i = 0; i < myName.length; i++) {
	if (
		myName[i].toLowerCase() == "a" ||
		myName[i].toLowerCase() == "e" ||
		myName[i].toLowerCase() == "i" ||
		myName[i].toLowerCase() == "o" ||
		myName[i].toLowerCase() == "u"
	) {
		console.log(3);
	}
	else {
		console.log(myName[i]);
	}
} 
