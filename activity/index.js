console.log("Hello, World!");

let number = Number(prompt("Give me a number greater than 50"));

console.log("The number you provided is " + number + ".");

for (number; number > 50; number--) {
	
	if (number % 10 == 0) {
		console.log("The number is divisible by 10. Skipping the number");
	}
	else if (number % 5 == 0) {
		console.log(number);	
	}
}
console.log("The current value is 50. Terminating the Loop.")

// ==============================================================

let word =  "supercalifragilisticexpialidocious";
let consonants = [];

for (let i = 0; i < word.length; i++) {
	if (
		word[i] !== 'a' &&
		word[i] !== 'e' &&
		word[i] !== 'i' &&
		word[i] !== 'o' &&
		word[i] !== 'u' 
	) {
		consonants.push(word[i]);
	}
}
console.log(word);
console.log(consonants.join(""));
